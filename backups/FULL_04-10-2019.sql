PGDMP         ,            	    w            oms %   10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)    11.5     g           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            h           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            i           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            j           1262    16574    oms    DATABASE     m   CREATE DATABASE oms WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C.UTF-8' LC_CTYPE = 'C.UTF-8';
    DROP DATABASE oms;
             postgres    false            k           0    0    DATABASE oms    ACL     '   GRANT ALL ON DATABASE oms TO anyosoft;
                  postgres    false    2922            �            1259    16600    company    TABLE     �  CREATE TABLE public.company (
    id uuid NOT NULL,
    created_by character varying(255),
    created_on timestamp with time zone,
    updated_on timestamp with time zone,
    name character varying(255),
    signatory_name character varying(255),
    corporate_address character varying(255),
    local_address character varying(255),
    poa_name character varying(255),
    poa_age integer,
    poa_number character varying(255),
    poa_email character varying(255),
    code character varying(50)
);
    DROP TABLE public.company;
       public         anyosoft    false            �            1259    16591    post    TABLE     �   CREATE TABLE public.post (
    id integer NOT NULL,
    text character varying(255) NOT NULL,
    text2 character varying(255) NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public.post;
       public         anyosoft    false            �            1259    16589    post_id_seq    SEQUENCE     �   CREATE SEQUENCE public.post_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.post_id_seq;
       public       anyosoft    false    199            l           0    0    post_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.post_id_seq OWNED BY public.post.id;
            public       anyosoft    false    198            �            1259    16617    section_138    TABLE       CREATE TABLE public.section_138 (
    id uuid NOT NULL,
    company_id uuid,
    unique_code character varying(255),
    lot_no character varying(255),
    loan_amount double precision,
    loan_account_no character varying(255),
    party_1_name character varying(255),
    party_1_address character varying(255),
    category character varying(255),
    parties_to_petition character varying(255),
    cheque_status character varying(255),
    cheque_bank character varying(255),
    cheque_drawn_on character varying(255),
    account_no character varying(255),
    cheque_no character varying(255),
    cheque_date timestamp with time zone,
    cheque_amount character varying(255),
    deposit_date timestamp with time zone,
    return_date timestamp with time zone,
    memo_date timestamp with time zone,
    court_name character varying(255),
    our_bank character varying(255),
    our_jurisdiction character varying(255),
    filing_date timestamp with time zone,
    ldoh timestamp with time zone,
    ldoh_status character varying(255),
    ndoh timestamp with time zone,
    case_number character varying(255),
    current_status character varying(255),
    late_status character varying(255),
    allocation_date timestamp with time zone,
    data_cross_check_date timestamp with time zone,
    notice_prepare_date timestamp with time zone,
    notice_dispatch_date timestamp with time zone,
    petition_ready_date timestamp with time zone,
    case_filing_date timestamp with time zone,
    office_file_preparation_date timestamp with time zone,
    cogniziance_date timestamp with time zone,
    bw_date timestamp with time zone,
    aw_date timestamp with time zone,
    misc json,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public.section_138;
       public         anyosoft    false            �            1259    16608    task_tracker    TABLE     �  CREATE TABLE public.task_tracker (
    id uuid NOT NULL,
    company_id uuid,
    case_type character varying(255),
    task character varying(255),
    loan_account_number character varying(255),
    customer_name character varying(255),
    task_type character varying(255),
    assigned_to character varying(255),
    task_assigned_date timestamp with time zone,
    task_closure_time timestamp with time zone,
    detail_remarks character varying(255),
    current_stage character varying(255),
    final_status character varying(255),
    task_closure_date timestamp with time zone,
    task_resolved_date timestamp with time zone,
    reason_of_delay character varying(255),
    critical boolean DEFAULT false,
    other_task_type character varying(255),
    other_case_type character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
     DROP TABLE public.task_tracker;
       public         anyosoft    false            �            1259    16577    users    TABLE     �  CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    mobile character varying(255),
    "profilePhotoUri" character varying(255),
    role character varying(255) DEFAULT 'USER'::character varying,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public.users;
       public         anyosoft    false            �            1259    16575    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       anyosoft    false    197            m           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       anyosoft    false    196            �
           2604    16594    post id    DEFAULT     b   ALTER TABLE ONLY public.post ALTER COLUMN id SET DEFAULT nextval('public.post_id_seq'::regclass);
 6   ALTER TABLE public.post ALTER COLUMN id DROP DEFAULT;
       public       anyosoft    false    198    199    199            �
           2604    16580    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       anyosoft    false    196    197    197            b          0    16600    company 
   TABLE DATA               �   COPY public.company (id, created_by, created_on, updated_on, name, signatory_name, corporate_address, local_address, poa_name, poa_age, poa_number, poa_email, code) FROM stdin;
    public       anyosoft    false    200   �,       a          0    16591    post 
   TABLE DATA               I   COPY public.post (id, text, text2, "createdAt", "updatedAt") FROM stdin;
    public       anyosoft    false    199   4       d          0    16617    section_138 
   TABLE DATA               �  COPY public.section_138 (id, company_id, unique_code, lot_no, loan_amount, loan_account_no, party_1_name, party_1_address, category, parties_to_petition, cheque_status, cheque_bank, cheque_drawn_on, account_no, cheque_no, cheque_date, cheque_amount, deposit_date, return_date, memo_date, court_name, our_bank, our_jurisdiction, filing_date, ldoh, ldoh_status, ndoh, case_number, current_status, late_status, allocation_date, data_cross_check_date, notice_prepare_date, notice_dispatch_date, petition_ready_date, case_filing_date, office_file_preparation_date, cogniziance_date, bw_date, aw_date, misc, "createdAt", "updatedAt") FROM stdin;
    public       anyosoft    false    202   .4       c          0    16608    task_tracker 
   TABLE DATA               U  COPY public.task_tracker (id, company_id, case_type, task, loan_account_number, customer_name, task_type, assigned_to, task_assigned_date, task_closure_time, detail_remarks, current_stage, final_status, task_closure_date, task_resolved_date, reason_of_delay, critical, other_task_type, other_case_type, "createdAt", "updatedAt") FROM stdin;
    public       anyosoft    false    201   K4       _          0    16577    users 
   TABLE DATA                  COPY public.users (id, name, email, username, password, mobile, "profilePhotoUri", role, "createdAt", "updatedAt") FROM stdin;
    public       anyosoft    false    197   �=       n           0    0    post_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.post_id_seq', 1, false);
            public       anyosoft    false    198            o           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 13, true);
            public       anyosoft    false    196            �
           2606    16607    company company_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.company DROP CONSTRAINT company_pkey;
       public         anyosoft    false    200            �
           2606    16599    post post_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.post DROP CONSTRAINT post_pkey;
       public         anyosoft    false    199            �
           2606    16624    section_138 section_138_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.section_138
    ADD CONSTRAINT section_138_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.section_138 DROP CONSTRAINT section_138_pkey;
       public         anyosoft    false    202            �
           2606    16616    task_tracker task_tracker_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.task_tracker
    ADD CONSTRAINT task_tracker_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.task_tracker DROP CONSTRAINT task_tracker_pkey;
       public         anyosoft    false    201            �
           2606    16588    users users_email_key 
   CONSTRAINT     Q   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);
 ?   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_key;
       public         anyosoft    false    197            �
           2606    16586    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         anyosoft    false    197            b     x��W]o�6}�~�3X3%)R����J�ĖIΠE_(~�=�؆�4�}��K��s�%��=I��3��.
�Е�s�=��PqF�՘3�p"j���qlC_s��~���	&	�Q�-��
q%���|,t���5�e*�h\�G�����E�r�^o{Ha���v���G�B�����7����~��km�wwn{����bs�E��4 z������.FUZ�DU�Y��Br����I�d��j�i	�%����ﰴ��(�ql}���TR�*$v�F
Mo��Ñ���P�����7H��j� �ҙ5pt�h��o�~��z?�]�w���D�h��U��_8hYm�18�4����`Gޑ�	BbVy�5�[����H�Q���~Z~H=,�r��E���*������R�Y�zp&�AE�����/�o��F�G; x�Ȃ$q�!�K�"	!cͿܤ�!���DZ�#�#�]��3﹯cIl���Y5��
�I���^��x�ƣ	�x��"� �b=���?�ᷟUPo ����;�_o7��;���}��GnRZ��_�����p�v����CW�{��ѻ=PyW�F�¿k��Q��Z@��Ez�_E!��0lw��Z �����fy��9Ò�i\�:��1~��T�R����
�����[���/�,T����ӫ�Mj�n����/�f����b7oN��hqS���%�W<#S�i��r���:��*%c�o`���;7�l[���8����(�2�{�|V�����r��7}V�~ k�ʝ��b��_jtq�ڨ砫+����V�э��M���`s\��"��U(�j���Ж��R-��3�m�
"g�j��I��:3�a�bh���/ǁ��� U��"^��:q����Z˯i��m��E:���Jl���]:���D/�h�\�Q�����'t��w=t��B�=��TB���c?͜�B��ѽ?4_4q���{�9P�G�{m�j��5oG7�D7��*�x�m�	t��e�	�����S	��Sb��&�IhdR��|fY[�Ӵ*�ѫ��䫴?|}X�Y3�H@�����࠙n9Y���a�4�H(�h��
O��9T�~�r�{r�?��I�����j��Aó����X303,�On��6��\�Z�~A��P�ó��5M��D�c���0�#���<Dh�e��EqB:f���HO3�	��P��{<�M��:���X4g�c���cZ�v��8�ae�6����E^����T�}�2����}hB�'�XJ+1�	���d�U"IN醎@���c��5�Q$���X����)��>�}�,�$�D��	�������cM�i\Ͱ��S��f��F""����ACO`ߘ�&�P�P�]l�il��pp�	����������x�6J]�8Y׆I�E�F�	Ӄq��b�pU (���~��fs����(12�9�{ӭ/"N鄚�yv��8�n�����xC@�D��̱�����k���݅�J�~�C\Mԧ	j/X3����"b���i01�3I���N��P:�rX�
5AU��r��#G�|2U�w��o�w:2��1��n�"�m�a�XG�9�Dq�uC�?��/���H�H��M�XX0q.i����F��Qw�r�ɻ�χY����e�m(�a(X(�0t� 9�u��31X*#֑.�����@'y��;]��Y5ʳ�[]M.�q`���B=���X�_h�E	���X�埁Q�N)�y�	X��:@���� ~�x�      a      x������ � �      d      x������ � �      c   z	  x��YM��H=�_�i/����ç�1�Ŕm,�U��H�L /{����H쪲�q�dٔ�ED�|�"�s;�0c��w�	ɑv���YY���Fށ�c�#�q���J���h_6E�GYU�u�=��K\>ŧo�Mڤ��m��6��(�缊$�"���"*�j-̚��p�o�?M:&zMXcL0��v���<{�p�?�u���C���mY�������{����r�'T�+�Ӫ�^p܃M2���9�4b�:�p�$dtY���;@"D�=����p㧲����gi�G����W �f��`�G�3�������[���M�3�=��lʪ�?�VWK�kLל$ذ��R2AzIh_�<�yp�L�)�|mZ��H�i�`�q���$@�:��0�`��,�z����m^����������>��c��05�3ώ�(`hF� @�hz��w���U����z����5M��>bbBה�O��E*)��kā��[�"K�B�y૰�6jI+�IL,��N /�R�{Ț�"`����<��<����,N�?�u��M\�&�}�}��׌�k�O!�xI3�������bo :*�X'l�ߗ%�=$��h�Ow��r�Q�Xk0�PI�Rc9�ζ�0�A"�z���r([N�&-W�8�mO	���!�6iU��#\�("�1fDpJ�}
y�_����Lߤ��
*@ �^�h�
�L�(Ӧ��9`)�n^�,m�K��p߇�yr�L��,5K�
nW�2������5YO[�.�4q�m#�,��됤^B�2
9�ID���`��n��P�6{��VU�o�m	�"$[���n���2�j��5�8ٸ:���V��u+�q��!��A�'M�L%��&�L�9
;U�Rj�k�M���-�-���L"e��mk[� W�m�m��q����vy�CaPC�X���w�y�ӇV�:V���*�z�@�P�:��4���7���!!��h��!|Y�"��x�-��G�$^��x�]�x]��V<S��Ef��k�e����WI�!�	��J�RB$�/C+?D��3��l�"m�%��;������L�P�6E}H��y^�̥�ɡJ`��R�ڴ�V�֓i��=(���ꘁ���Z�6�U�vѨP���G���R�Ӡ(� t�o����y��nph�:���5��@�9`
�^sBs"��M+�:Zb\-�>gl �3۪^���٦Pl��t���!��t@�7���sZ�	Uz����%��P1�uO
�QI�$�&Ž^���!��DbJ%f��kӪS�Ēc$0��V�Ԇ&˘�Y��hےn���[�!�h�:e��Z�I'~+�gX�[���j�B���M��A'�7U�5��nL&` ,$����l�#Y���B���/M���(��;ǡ����$乒�J/�޹e�>%�P��mZ�g��j��沬<��<U@������qUG��˾|�|�l�i����Ϗ����O��i&�C�8_�V�'m��Cu�[k�A�[�M����w;ч�p�q�Ŋ��rwsp�K`��C��݊N	�8����D��~mZy�=�9C0.��s�az�9j���*C�zӓ.v��V$*S���r�����I��r���p����a�9��p-s�Բ��4���� K�>�8JE_ݝ��^`Mp�Cf��4�h'��è��w��xz��m�w��9>�Z%�q�*�K��l�h_8}7p�J�n���Rkj{D�!�;�!M�G�k�h��0v�D9�Dx�E���2�Pjv���G�1A��g��0�:��BMaPb<���rP(Bpg��h�\�̒�|�9�lӜ�5�t���u�D[��R6�)�"�=T���Y�(�P��rw�A��IB����6�' .ޏ�M�җ8����0�tc3L�4�0J9M���q�BCJ#ٻV
�0r�֧"Dw����N���"a���S1�]ڍ%H
�d��-F^�~t�u
@���k�&���1#9�.y�A['�#z�#{8og�����h81���OS]����P�{�8���gbZY����*Y+�zX6�&�J�#����Y� '�٨�`s��o"�ל' 5g�0�z����ASu=�2e)j�m-�Pq{�[�h�Oh���ǑAߖ	����������p��䚁����ð� 
JH�B�JeP�0�[�z���`.��6e�3�孷�|��_�e}��������s��i�	U�� ��pFv	կ�����ǝ`l�<�L�4���t��"~�M8��@B�Q��)x�qx����ڶȱ����ҘZ�� #��é�%��x?5��G_��%�N�t��dU^���O\'؄�[�(����u�PZ��`%��g%�����i,'      _   �  x�}�K��8�5|
���!	oW�""(_u7iE�O?�S�#5^����_���`J�d�3�Ɂd�@6U�ῷI� ̿v ��&~�z���HXb��ǹ>D�*KI��K�6X{n�G+����n0�~[��w��!�!��$v1��0�e��o"��&Ɂ��8���?�l[�6ʃ�'�`�k�I���y�4��-�e"k1�k���v������zS�o���@�}�|�h�2n�9錿�Y��ɵ�\�Ե�з�c�%?<]��*,.�@��٢`��f��rZ��MU��S1XDo��`l��\����a��6��1�[�"��{b�%� ��}�"�+��r�e����m%��g�76�e�0���e�*�t&y�y��Ph��Po��ƹn��#�No� w����b�F>��9YX�L��<���z{{�D���`E�_5�	-R��4�'�mbc���1���:�㻌�ϝ(:;�v��ՅqȽ��ɨgm�R�IzzS=��X��)�%�_$�w�~�Wj�F�C7s\TKuV��c��v�v)�'(���H�>" p֧�Y��/s�# %��'�ej�ec��(Qt| �6Rn��r5�|�}�W��~
:#c�r��M���N�QX/�R�Y��g/&ջ�F���<E4B��₤�- �0,���q����;͢������^�t��l�d��Eb���
�r�o���<F/�O�0����P[��cb���#[1Pi�oO��>�6L�Y��Q1򇊶S�a�ɻ���,���D4b)��
���?	��IN|�EZ�J�9��Z��l���&��a}|�s~��B��������?�X�Ep�� ?G4���(���l�1��q��8
���n�L^�`̭�&	���@���Q���D(����@s�ϧ�9H�B	�W�̢. ���߀�� ��e     