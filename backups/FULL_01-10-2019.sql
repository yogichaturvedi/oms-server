PGDMP     !        	        	    w            oms %   10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)    11.5     _           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            `           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            a           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            b           1262    16426    oms    DATABASE     m   CREATE DATABASE oms WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C.UTF-8' LC_CTYPE = 'C.UTF-8';
    DROP DATABASE oms;
             postgres    false            c           0    0    DATABASE oms    ACL     Q   REVOKE ALL ON DATABASE oms FROM postgres;
GRANT ALL ON DATABASE oms TO anyosoft;
                  postgres    false    2914            �            1259    16443    case    TABLE     �  CREATE TABLE public."case" (
    id uuid NOT NULL,
    created_on timestamp with time zone,
    updated_on timestamp with time zone,
    created_by character varying(255),
    company_id uuid,
    case_type character varying(255),
    company_name character varying(255),
    borrower_name character varying(255),
    co_borrower_name character varying(255),
    gr_name character varying(255),
    registration_number character varying(255),
    engine_number character varying(255),
    chassis_number character varying(255),
    "current_date" timestamp with time zone,
    receiver_name character varying(255),
    date_of_allocation timestamp with time zone,
    data json
);
    DROP TABLE public."case";
       public         anyosoft    false            �            1259    16435    company    TABLE     �  CREATE TABLE public.company (
    id uuid NOT NULL,
    created_on timestamp with time zone,
    updated_on timestamp with time zone,
    created_by character varying(255),
    name character varying(255),
    signatory_name character varying(255),
    corporate_address character varying(255),
    local_address character varying(255),
    poa_name character varying(255),
    poa_age character varying(255),
    poa_number character varying(255),
    poa_email character varying(255)
);
    DROP TABLE public.company;
       public         anyosoft    false            �            1259    16429 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    run_on timestamp without time zone NOT NULL
);
    DROP TABLE public.migrations;
       public         anyosoft    false            �            1259    16427    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public       anyosoft    false    197            d           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
            public       anyosoft    false    196            �            1259    16451    section-138    TABLE       CREATE TABLE public."section-138" (
    id uuid NOT NULL,
    created_on timestamp with time zone,
    updated_on timestamp with time zone,
    created_by character varying(255),
    company_id uuid,
    unique_code character varying(255),
    lot_no character varying(255),
    loan_account_no character varying(255),
    loan_amount double precision,
    party_1_name character varying(255),
    party_1_address character varying,
    category character varying(255),
    parties_to_petition character varying(255),
    cheque_status character varying(255),
    cheque_bank character varying(255),
    cheque_drawn_on character varying(255),
    account_no character varying(255),
    cheque_no character varying(255),
    cheque_date timestamp with time zone,
    cheque_amount character varying(255),
    deposit_date timestamp with time zone,
    return_date timestamp with time zone,
    memo_date timestamp with time zone,
    court_name character varying(255),
    our_bank character varying(255),
    our_jurisdiction character varying(255),
    filing_date timestamp with time zone,
    ldoh timestamp with time zone,
    ldoh_status character varying(255),
    ndoh character varying(255),
    case_number character varying(255),
    current_status character varying(255),
    late_status character varying(255),
    allocation_date timestamp with time zone,
    data_cross_check_date timestamp with time zone,
    notice_prepare_date timestamp with time zone,
    notice_dispatch_date timestamp with time zone,
    petition_ready_date timestamp with time zone,
    case_filing_date timestamp with time zone,
    office_file_preparation_date timestamp with time zone,
    cogniziance_date timestamp with time zone,
    bw_date timestamp with time zone,
    aw_date timestamp with time zone,
    misc json
);
 !   DROP TABLE public."section-138";
       public         anyosoft    false            �            1259    16459    task_tracker    TABLE     `  CREATE TABLE public.task_tracker (
    id uuid NOT NULL,
    created_on timestamp with time zone,
    updated_on timestamp with time zone,
    created_by character varying(255),
    company_id uuid,
    case_type character varying(255),
    task character varying,
    loan_account_number character varying(255),
    customer_name character varying(255),
    task_type character varying(255),
    assigned_to character varying(255),
    task_assigned_date timestamp with time zone,
    task_closure_time timestamp with time zone,
    detail_remarks character varying,
    current_stage character varying(255),
    final_status character varying(255),
    task_closure_date timestamp with time zone,
    reason_of_delay character varying,
    other_task_type character varying(255),
    other_case_type character varying(255),
    critical boolean DEFAULT false
);
     DROP TABLE public.task_tracker;
       public         anyosoft    false            �
           2604    16432    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public       anyosoft    false    197    196    197            Z          0    16443    case 
   TABLE DATA                 COPY public."case" (id, created_on, updated_on, created_by, company_id, case_type, company_name, borrower_name, co_borrower_name, gr_name, registration_number, engine_number, chassis_number, "current_date", receiver_name, date_of_allocation, data) FROM stdin;
    public       anyosoft    false    199   :)       Y          0    16435    company 
   TABLE DATA               �   COPY public.company (id, created_on, updated_on, created_by, name, signatory_name, corporate_address, local_address, poa_name, poa_age, poa_number, poa_email) FROM stdin;
    public       anyosoft    false    198   W)       X          0    16429 
   migrations 
   TABLE DATA               6   COPY public.migrations (id, name, run_on) FROM stdin;
    public       anyosoft    false    197   �-       [          0    16451    section-138 
   TABLE DATA               �  COPY public."section-138" (id, created_on, updated_on, created_by, company_id, unique_code, lot_no, loan_account_no, loan_amount, party_1_name, party_1_address, category, parties_to_petition, cheque_status, cheque_bank, cheque_drawn_on, account_no, cheque_no, cheque_date, cheque_amount, deposit_date, return_date, memo_date, court_name, our_bank, our_jurisdiction, filing_date, ldoh, ldoh_status, ndoh, case_number, current_status, late_status, allocation_date, data_cross_check_date, notice_prepare_date, notice_dispatch_date, petition_ready_date, case_filing_date, office_file_preparation_date, cogniziance_date, bw_date, aw_date, misc) FROM stdin;
    public       anyosoft    false    200   =.       \          0    16459    task_tracker 
   TABLE DATA               K  COPY public.task_tracker (id, created_on, updated_on, created_by, company_id, case_type, task, loan_account_number, customer_name, task_type, assigned_to, task_assigned_date, task_closure_time, detail_remarks, current_stage, final_status, task_closure_date, reason_of_delay, other_task_type, other_case_type, critical) FROM stdin;
    public       anyosoft    false    201   Z.       e           0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 1, true);
            public       anyosoft    false    196            �
           2606    16450    case case_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public."case"
    ADD CONSTRAINT case_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public."case" DROP CONSTRAINT case_pkey;
       public         anyosoft    false    199            �
           2606    16442    company company_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.company DROP CONSTRAINT company_pkey;
       public         anyosoft    false    198            �
           2606    16434    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public         anyosoft    false    197            �
           2606    16458    section-138 section-138_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public."section-138"
    ADD CONSTRAINT "section-138_pkey" PRIMARY KEY (id);
 J   ALTER TABLE ONLY public."section-138" DROP CONSTRAINT "section-138_pkey";
       public         anyosoft    false    200            �
           2606    16467    task_tracker task_tracker_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.task_tracker
    ADD CONSTRAINT task_tracker_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.task_tracker DROP CONSTRAINT task_tracker_pkey;
       public         anyosoft    false    201            Z      x������ � �      Y   y  x��V�n�F]�_qWE�rܙ�fG[�%ۢJV6�\�C��D
$e�Y��i����%�C�v�)
'E ��y�{�9���WR�s�L�ܰ�K=Vx"r�HU�����		B�U���������z΢��
.&q���f1������Up���Ɓ����eSV9>b�r��|]V��?��BRcN�6�9����]���H���\:�,F�1L���.����+]��(���B�"-r�xa��U���s�Y(�� r׶��Ѯ�᢬��L��D�}�N���(��9��9�W�vKa¹�:C1�MV2G`n�Ǯ�[�V�>���B=����{�&��~<.�_':��,ca!\�
���$g~a�+=���>�i4���$�L>��N���&C�����1{7���]r��%�C-�5$�hHEj{���1[� -a��y�aHιR~*ϕ�~�и(��s���gʄKy�3Y�H�,�A���U]w0�7�(+x�s�G�)7egr�ˋ� ��!�w�����{D�Gq8pSw���f[7��A����(`oZ�K�_��i[���=���eݘ{��ێ0�mR,(��w�׶�I2�3�PaΒѡ2���]�OIV��UJ�J2-2�D��4Ph�p_��v�-;\K����i�̴O�釮\�6X� l(Ѧ�⺄��ֆT
�ve�Q�̖�Ρ��q��b���d���^h-��+=�DN��(�9C�)��4B?(���^�zg��YҾ K�
-�=�3筩ʪݭ��D˓�.��b�`ט����l��h�Y?���[���eS�^�G��U+�\�3 X$�!�X�ƓŁ��v]��ZHM�e9��Rn��3���Q��.�s8OFC�z>J���՞�]d|%���k�]�t�5�`�3���C�7d�Gb��9XU�vl�5͇�w��tM�̃��ȯ�U�N�=�\N��9\�M�d�,����H���/M�"0Lh�g%���bY��n�}Wx�����t2�����3��V�/9�I{49�V�$M���e޴̻z_Y���{��}�<ZoWD����3/B��a��`C����X�i��v{z,��5~D�89���aR�V�J����h�I|9�r`O�P�^ޟ�����ڲ      X   M   x�3��72047054407172�M/O�I����,�L����L/J,����*��5��54Q04�22�25�34����� �5�      [      x������ � �      \   	  x��YMs�:]�_��l�����Q�Ӹcp�6ɤ�UMI������5W�`L�������{���{�0c�!��ɑ�C��;����"*�j)̒��j�O��[�k�pDI1���s�{�T�B��P��Vm�勬��&��y��T/���de[���ʶx+��O�v�׋Ɩ���@R��<���VScL\��q�V�=O6��?��]���EVVM��7�x.ʲ���~�hpq���y�	ӈ1��$!g���%3)�dJ�d�^�j��6�D�o�9Oj[ �䥪�l�_N2����SVlk��f�``��~&M���N�4�������n�6�TmU7ɑ�ݶ(/s�b�No�����6G����^;�1��3�X H0 ��3a&
 � �|����%eK�S��#,7y��{���e�$��yS4MQm�b�dծng���;�v-�i���~�d��o3�Y��-��Pl��VW?�iN鄟�L*)��k�A��;�!G�B�P�p���\
�%M�l����3�G�v�� �IL��^� �R=�%�ea#'��o��%�Թ]}&Y����]�&v�J����7Ϡ��p��#b!�����}~]�>�)����
�9�;E�7��q$��a�7��]�,9TI����,�H�]�*H�)�@��i�Q�wO��n��-o�^ٺ��v��g�PcF�t�� �u����}e� )���^J�v��	DpV���u]���@ T�]�W�m�P��x��t�	Et ����P ��ć���_r�R&��� O�*���1G��{$i���B���VL�c�_�:��
�'����M�4{F,����fF��*��j��ݖ�M���ͺH~V���D�R�T�IC�V�w��*�h2����:;��h��h�#�C��hd8�H9���L�l����i����T�G��^A0��e{V��M~�������q���(�^�I��P�z��4�B0��!!T�頄!|jJx)x
�;C�^�*�^2�YRƚ���!��q|�>�|���Yɞ,����bH� EM(FNP��8��4x(vD�b<���ҽdA�=�1�Y�מ,��:�=�|�����Ͷ��"���9=�@:���v� A7��g�z�dӞ��$��>Y�����Q�T(�;< g�M�kȧ�9�]��?~��#l�iNG߰�c�ed�q�l�o7e�0�Lջ�"�n��<荇��K���� ��
0Y
��K%M�PRi�r�0��A�+�ũ��'���7 ����?�?`���mJ��	7<v��� 6��Z���p^�?���Xr��&�I��B�0'�gn
�-il3���\���H�.�;�#�'�{e�
Z��Q�k���U��y�Y�y1g��mm���٤��u]�_3a(;�2eW��`�YA7@����=���A��X�J*'��陸��0��޳�{���Ѿ���(�,��⥀�-��>�+/E��G�%�eR7���u[}l�~_��M15g����������n�����aS�u�Ăc�]L�S�2�m.��ҽ4�x���?v�C�kN�祮6WG�{�q��y뢗��5��8C0J�s�aB8����@��LM�E@��3��.=J�>��8�/<��0=�;ty����uO�6bp��!Nl�#h��0��O��ђ�ԐfΖ�m���?ƀ�x��/Rx��8��rh/���4� ����Z�f|
f�R�f�9]�s~fW�Ɏ�u�+�T�ra�k�v���ʤ��^i$�@�ʡ�Rk�D�!�{�#MY@��蠩0�m'�L�iF�O���r��xMc�J�ăE���.��FOϽ�S��(�T�!����j�_2���������������m��ahc�<�	�����~�V-fJ��*[0��[<AȢoe��,b��6/F�@>����;q�x�u��0�k�
Z�=�DyM���q�AeC�#9�N�<��섂��HJ�9�Kw������Vp�t��1)�g����oS,D�Q�D���`�Nyf$G�`(� �y�1h�,�	@qJ'������a'�~sܒ��c\��1q�.�u� ��0�N .8< =\�'adp�_r�B����t��V�̭&:��X��7�����`�A@�j�]���{E]p��Lt���K�j����)e�y���{J���q{*S<)�Kn��%�#�C��d� A��g}��Ҙ:�
���<LA�X?ZO�K�fIA>4UB��|��t�������w�K蔬���p��H�_����� ]H��     