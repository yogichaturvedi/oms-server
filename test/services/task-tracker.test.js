const assert = require('assert');
const app = require('../../src/app');

describe('\'task-tracker\' service', () => {
  it('registered the service', () => {
    const service = app.service('task-tracker');

    assert.ok(service, 'Registered the service');
  });
});
