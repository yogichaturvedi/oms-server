const assert = require('assert');
const app = require('../../src/app');

describe('\'section-138\' service', () => {
  it('registered the service', () => {
    const service = app.service('section-138');

    assert.ok(service, 'Registered the service');
  });
});
