// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const section138 = sequelizeClient.define('section_138', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      unique: true,
      isUUID: 4,
      defaultValue: DataTypes.UUIDV4
    },
    companyId: {field: "company_id", type: DataTypes.UUID},
    uniqueCode: {field: "unique_code", type: DataTypes.STRING},
    lotNo: {field: "lot_no", type: DataTypes.STRING},
    loanAmount: {field: "loan_amount", type: DataTypes.FLOAT},
    loanAccountNo: {field: "loan_account_no", type: DataTypes.STRING},
    party1Name: {field: "party_1_name", type: DataTypes.STRING},
    party1Address: {field: "party_1_address", type: DataTypes.STRING},
    category: {field: "category", type: DataTypes.STRING},
    partiesToPetition: {field: "parties_to_petition", type: DataTypes.STRING},
    chequeStatus: {field: "cheque_status", type: DataTypes.STRING},
    chequeBank: {field: "cheque_bank", type: DataTypes.STRING},
    chequeDrawnOn: {field: "cheque_drawn_on", type: DataTypes.STRING},
    accountNo: {field: "account_no", type: DataTypes.STRING},
    chequeNo: {field: "cheque_no", type: DataTypes.STRING},
    chequeDate: {field: "cheque_date", type: DataTypes.DATE},
    chequeAmount: {field: "cheque_amount", type: DataTypes.STRING},
    depositDate: {field: "deposit_date", type: DataTypes.DATE},
    returnDate: {field: "return_date", type: DataTypes.DATE},
    memoDate: {field: "memo_date", type: DataTypes.DATE},
    courtName: {field: "court_name", type: DataTypes.STRING},
    ourBank: {field: "our_bank", type: DataTypes.STRING},
    ourJurisdiction: {field: "our_jurisdiction", type: DataTypes.STRING},
    filingDate: {field: "filing_date", type: DataTypes.DATE},
    ldoh: {field: "ldoh", type: DataTypes.DATE},
    ldohStatus: {field: "ldoh_status", type: DataTypes.STRING},
    ndoh: {field: "ndoh", type: DataTypes.DATE},
    caseNumber: {field: "case_number", type: DataTypes.STRING},
    currentStatus: {field: "current_status", type: DataTypes.STRING},
    lateStatus: {field: "late_status", type: DataTypes.STRING},
    allocationDate: {field: "allocation_date", type: DataTypes.DATE},
    dataCrossCheckDate: {field: "data_cross_check_date", type: DataTypes.DATE},
    noticePrepareDate: {field: "notice_prepare_date", type: DataTypes.DATE},
    noticeDispatchDate: {field: "notice_dispatch_date", type: DataTypes.DATE},
    petitionReadyDate: {field: "petition_ready_date", type: DataTypes.DATE},
    caseFilingDate: {field: "case_filing_date", type: DataTypes.DATE},
    officeFilePreparationDate: {field: "office_file_preparation_date", type: DataTypes.DATE},
    cognizianceDate: {field: "cogniziance_date", type: DataTypes.DATE},
    bwDate: {field: "bw_date", type: DataTypes.DATE},
    awDate: {field: "aw_date", type: DataTypes.DATE},
    misc: {field: "misc", type: DataTypes.JSON},
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  section138.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return section138;
};
