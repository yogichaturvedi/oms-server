// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const taskTracker = sequelizeClient.define('task_tracker', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      unique: true,
      isUUID: 4,
      defaultValue: DataTypes.UUIDV4
    },
    companyId: {field: "company_id", type: DataTypes.UUID},
    caseType: {field: "case_type", type: DataTypes.STRING},
    task: {field: "task", type: DataTypes.STRING},
    loanAccountNumber: {field: "loan_account_number", type: DataTypes.STRING},
    customerName: {field: "customer_name", type: DataTypes.STRING},
    taskType: {field: "task_type", type: DataTypes.STRING},
    assignedTo: {field: "assigned_to", type: DataTypes.STRING},
    taskAssignedDate: {field: "task_assigned_date", type: DataTypes.DATE},
    taskClosureTime: {field: "task_closure_time", type: DataTypes.DATE},
    detailRemarks: {field: "detail_remarks", type: DataTypes.STRING},
    currentStage: {field: "current_stage", type: DataTypes.STRING},
    finalStatus: {field: "final_status", type: DataTypes.STRING},
    taskClosureDate: {field: "task_closure_date", type: DataTypes.DATE},
    taskResolvedDate: {field: "task_resolved_date", type: DataTypes.DATE},
    reasonOfDelay: {field: "reason_of_delay", type: DataTypes.STRING},
    critical: {field: "critical", type: DataTypes.BOOLEAN, defaultValue: false},
    otherTaskType: {field: "other_task_type", type: DataTypes.STRING},
    otherCaseType: {field: "other_case_type", type: DataTypes.STRING},
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  taskTracker.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return taskTracker;
};
