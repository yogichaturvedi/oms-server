// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const company = sequelizeClient.define('company', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      unique: true,
      isUUID: 4,
      defaultValue: DataTypes.UUIDV4
    },
    createdBy: {
      field: "created_by",
      type: DataTypes.STRING
    },
    createdAt: {
      field: "created_on",
      type: DataTypes.DATE
    },
    updatedAt: {
      field: "updated_on",
      type: DataTypes.DATE
    },
    name: {
      type: DataTypes.STRING
    },
    signatoryName: {
      field: "signatory_name",
      type: DataTypes.STRING
    },
    corporateAddress: {
      field: "corporate_address",
      type: DataTypes.STRING
    },
    localAddress: {
      field: "local_address",
      type: DataTypes.STRING
    },
    poaName: {
      field: "poa_name",
      type: DataTypes.STRING
    },
    poaAge: {
      field: "poa_age",
      type: DataTypes.INTEGER
    },
    poaNumber: {
      field: "poa_number",
      type: DataTypes.STRING
    },
    poaEmail: {
      field: "poa_email",
      type: DataTypes.STRING
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  company.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return company;
};
