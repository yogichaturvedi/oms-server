// Initializes the `company` service on path `/company`
const {Company} = require('./company.class');
const createModel = require('../../models/company.model');
const hooks = require('./company.hooks');
const Companies = require("../../../master-data/companies");

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate,
    multi: ['create', 'patch'],
  };

  // Initialize our service with any options it requires
  app.use('/company', new Company(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('company');

  service.hooks(hooks);

  app.post("/company-save", function (request, response) {
    return service.create(Companies).then(da => {
      response.end("Created", da);
    }
    ).catch(error => {
      response.end("Failed", error);
    });
  });
};
