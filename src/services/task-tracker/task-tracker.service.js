// Initializes the `task-tracker` service on path `/task-tracker`
const { TaskTracker } = require('./task-tracker.class');
const createModel = require('../../models/task-tracker.model');
const hooks = require('./task-tracker.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/task-tracker', new TaskTracker(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('task-tracker');

  service.hooks(hooks);
};
