// Initializes the `section-138` service on path `/section-138`
const { Section138 } = require('./section-138.class');
const createModel = require('../../models/section-138.model');
const hooks = require('./section-138.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/section-138', new Section138(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('section-138');

  service.hooks(hooks);
};
