const users = require('./users/users.service.js');
const post = require('./post/post.service.js');
const company = require('./company/company.service.js');
const taskTracker = require('./task-tracker/task-tracker.service.js');
const section138 = require('./section-138/section-138.service.js');
const masterData = require('./master-data');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(post);
  app.configure(company);
  app.configure(taskTracker);
  app.configure(section138);
  app.configure(masterData);
};
