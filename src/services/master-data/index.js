'use strict';
const Status = require("../../../master-data/status");
const CaseTypes = require("../../../master-data/case-types");
const TaskTypes = require("../../../master-data/task-types");

module.exports = function () {
  const app = this;
  app.get("/master-data", function (request, response) {
    const payload = {
      status: Status,
      caseTypes: CaseTypes,
      taskTypes: TaskTypes,
    };
    return response.end(JSON.stringify(payload));
  });
};
